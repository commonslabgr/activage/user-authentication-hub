# User manual

## Description
The User Authentication Hub (UAH) by [commons|lab](http://commonslab.gr) is a Modular and Open-Source device based on open standards and communication protocols. It acts as an authentication service provider within a link-local network. It can provide three easy-to-use authentication mechanisms that do not require users to remember passwords. Users can authenticate using a physical key, their fingerprint and/or an NFC tag. Th current implementation of the device uses only the NFC tag. The authentication hub will be integrated to the ACTIVAGE IoT Ecosystem Suite (AIOTES). This offers an easy way to deploy the UAH to all current or future IoT platforms, which are integrated with AIOTES. It was proposed and developed under the 1st open-call of the H2020 [ACTIVAGE project](http://activageproject.eu).

## Device Setup
Set up the device network connection according to the support manual. 
Plug in device in power and wait to boot.
The device has holes in the back for wall mounting.

## Using UAH
### Isere Use Case
The LDAP server must be running when booting up the devices.

When the device has booted up and is running the following image will be shown on the device screen.

![alt text](img/running.png "Service is Running")

When a service in the local network requests user authentication from the Keycloak on the uah.local service the device will notify the user for a request to read their card by updating the device sceen with the message shown below.

![alt text](img/pls_swype.png "Please swype your card")

The user can place/swipe their card over the reader or choose to insert their card on the right side slot.
The card will be authenticated against the server and if correct the following message will be shown.

![alt text](img/success.png "Successfully authenticated")

Once the UAH has athenticated the user the service that had requested the user authentication will receive the appropriate authentication token.

In the case that the user can not be authenticated the following message will appear.

![alt text](img/error.png "Authentication error")


### Woquaz Use Case

When the device has booted up and is running the following image will be shown on the device screen.

![alt text](img/running.png "Service is Running")

UAH will check with UniversAAL for the current registered users. If new users are found that are not registered with cards the following message will appear on the device screen.

![alt text](img/new_user.png "New user registration")

A countdown timer of 10 seconds will give time to the user to swipe a new card for the user.
Then the following message will appear on the device screen.

![alt text](img/reg_success.png "Registration success")

When a user remains alone in the room they can swipe their card over their device and the following message will appear on the screen while notifying UniversAAL for the action.

![alt text](img/success.png "Successful authentication")

If the card read is not matched to a registered user the following message will appear on the device screen.

![alt text](img/unknown.png "Unknown user")


## Funding
This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 732679