# Support manual for the Open-Source User Authentication Hub

## Description
The User Authentication Hub (UAH) by [commons|lab](http://commonslab.gr) is a Modular and Open-Source device, based on open standards and on established communication protocols. It acts as an authentication service provider within a link-local network and can provide three easy-to-use authentication mechanisms, which do not require users to remember any passwords. Users can authenticate using a physical key, their fingerprint and/or an NFC tag. The current implementation of the device uses only the NFC tag. The User Authentication Hub will be integrated to the ACTIVAGE IoT Ecosystem Suite (AIOTES). This offers an easy way to deploy the UAH to all current or future IoT platforms, which are integrated with AIOTES. It was proposed for and developed under the 1st open-call of the Horizon2020 [ACTIVAGE project](http://activageproject.eu).


## Device Hardware
The UAH device consists of a Raspberry Pi 3 B+, a touch screen and an RFID reader, all fitted in a custom-designed case.
**Connection and wiring diagrams will be provided soon...**
The 3D design files for the custom-designed case can be found online in the STL file format at this location:
 https://gitlab.com/commonslabgr/activage/user-authentication-hub/tree/master/case

## Device Software
The software of the device consists of the Keycloak server, the User Authentication Hub middleware and some accompanying scripts.
The Raspberry Pi is running Raspbian stretch release v9.11.
The device's hostname is `uah` and can be accessed from any local network at `uah.local` domain.
It runs the following remotely accessible services:
* SSH: pi@uah.local
* Keycloak Administration: https://uah.local:8443/auth/
* UAH OpenID configuration: https://uah.local:8443/auth/realms/UAH/.well-known/openid-configuration
* UAH test site: https://uah.local:4443/
* UAH OpenID Access token: https://uah.local:4443/auth
* UAH OpenID ID token: https://uah.local:4443/token
* UAH OpenID User info: https://uah.local:4443/userinfo
* UAH OpenID Logout: https://uah.local:4443/logout

### Configuration
#### Network
Reconfigure the device to connect to your local WiFi network by adding the network details to the UAH.

Open the wpa-supplicant configuration file in /etc/wpa_supplicant/wpa_supplicant.conf:

Remove any previous network configuration and go to the bottom of the file and add the following:

```
network={
    ssid="testing"
    psk="testingPassword"
}
```


#### Keycloak server
Version: 6.0.1
Default location: /home/pi/keycloak-6.0.1/

The Keycloak server is executed as a standalone with the `bin/standalone.sh` script.

##### Accessing the Administration Console
Use a web browser to navigote to the [Keycloak Administration site](https://uah.local:8443/auth/) then enter the "Administration Console" with credentials Username: `admin` and password: `admin`. It is advised to change the password once you log in. To do this go to the "Admin" drop down menu on the far up and right corner and select "Manage account". From there you can change the password of the admin account.

##### Updating LDAP connection
Keycloak comes preconfigured to work with an existing LDAP server, to update this select the UAH realm, and then click on "User federation". A list of providers should show up with the only entry named "ldap". Click on the "ldap" link and then you can edit the "Connection URL" field. 
It may be appropriate to update other fields as well, such as the RDN LDAP attribute, the UUID LDAP attribute, User Object Classes or more, depending on your LDAP setup. The current configuration is set specifically for the Deployment Site ISERE of the ACTIVAGE project.


#### UAH middleware
The UAH middleware is a set of software programs:
* The HTTPS Server acts as a proxy to handle OpenID requests from clients to Keycloak and authenticate users using the NFC cards.
* The uah Controller acts as a message handler between the https server and the keycloak API and extra services for the UniversAAL platform.
* The handler program handles the user interface on the touch screen of the device by displaying messages and errors.
* The card reader program handles requests for the NFC scanner.

##### UAH configuration
The UAH middleware can be configured by editing the `config.py` in the `/home/pi/user_authentication_hub-middleware/` folder.

* set DeploymentSite as ISERE or WOQUAZ depending on the use case
  * ISERE specific for notifying Sensinact for card removal
    * sensinact_ip
    * sensinact_port
    * home_id
  * WOQUAZ specific for communication with UniversAAL
    * JsonHeader
    * TextHeader
    * Auth
    * target_uSpace
    * caller_instanceID
    * publisher_instanceID
    * serverIP
    * serverPort
    * base_dir
    * data_dir_space
    * data_dir_caller
    * data_dir_publisher
    * data_dir_caller_request
    * data_dir_published_messsage

##### UI Localization
The UI of UAH supports localization. So all texts can be translated to different languages.
We are using the [GNU gettext](https://www.gnu.org/software/gettext/) module in python.
All the strings in the `hanlder.py` are marked with an "_" e.g. `print(_("Hello world"))`.
We generate the base .pot file and then you can edit them using [poedit](https://poedit.net/) and generate the .mo files.
The translation files should be saved in folder `ui/locale/language/LC_MESSAGES/`

## Funding
This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 732679

## License
This document is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)